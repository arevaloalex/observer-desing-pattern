package observerpattern
import java.io.File
import java.util.ArrayList;
import java.util.HashMap;

class EventManager(vararg operations: String) {
    internal var listeners: MutableMap<String, ArrayList<EventListener>> = HashMap<String, ArrayList<EventListener>>()

    init {
        for (operation in operations) {
            this.listeners[operation] = ArrayList<EventListener>()
        }
    }

    fun subscribe(eventType: String, listener: EventListener) {
        val users = listeners[eventType]
        users!!.add(listener)
    }

    fun unsubscribe(eventType: String, listener: EventListener) {
        val users = listeners[eventType]
        users!!.remove(listener)
    }

    fun notify(eventType: String, file: File) {
        val users = listeners[eventType]
        for (listener in users!!) {
            listener.update(eventType, file)
        }
    }
}

class Editor {
    var events: EventManager
    private var file: File? = null

    init {
        this.events = EventManager("open", "save")
    }

    fun openFile(filePath: String) {
        this.file = File(filePath)
        events.notify("open", file!!)
    }

    @Throws(Exception::class)
    fun saveFile() {
        if (this.file != null) {
            events.notify("save", file!!)
        } else {
            throw Exception("Please open a file first.")
        }
    }
}

interface EventListener {
    fun update(eventType: String, file: File)
}


class EmailNotificationListener(private val email: String) : EventListener {

    override fun update(eventType: String, file: File) {
        println("Email to " + email + ": Someone has performed " + eventType + " operation with the following file: " + file.name)
    }
}

class LogOpenListener(fileName: String) : EventListener {
    private val log: File

    init {
        this.log = File(fileName)
    }

    override fun update(eventType: String, file: File) {
        println("Save to log " + log + ": Someone has performed " + eventType + " operation with the following file: " + file.name)
    }
}



object Demo {
    @JvmStatic
    fun main(args: Array<String>) {
        val editor = Editor()
        editor.events.subscribe("open", LogOpenListener("/path/to/log/file.txt"))
        editor.events.subscribe("save", EmailNotificationListener("admin@example.com"))

        try {
            editor.openFile("test.txt")
            editor.saveFile()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}